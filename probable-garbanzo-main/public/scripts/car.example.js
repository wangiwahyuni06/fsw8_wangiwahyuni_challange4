class Component {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
    withDriver,
  }) {
    if (this.constructor === Component) {
      throw new Error("Cannot create new instance from an abstract class")
  }
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
    this.withDriver = withDriver;
  }

  render() {
    return `
    <div class="col-lg-4" style="margin-bottom:2%; margin-top: 5%">
      <div class="card h-100 mb-2">
        <div class="card-body">
          <img src="${this.image}" style="width: 20rem; height:20rem">
          <h5 class="card-title mt-4" style="font-weight: 400; font-size: 16px">${this.manufacture}/${this.model}</h5>
          <p style="font-weight:bold;">Rp.${this.rentPerDay}/hari</p>
          <p class="card-text">${this.description}</p>
          <li class="list-fi mb-3" style="list-style-type: none;">
            <img src="images/fi_users.png" style="width: 7%;">
            ${this.capacity} Orang</li>
          <li class="list-fi mb-3" style="list-style-type: none;">
            <img src="images/fi_settings.png" style="width: 7%;">
            ${this.transmission}</li>
          <li class="list-fi mb-3" style="list-style-type: none;">
            <img src="images/fi_calendar.png" style="width: 7%;">
            Tahun ${this.year}</li>
          <a href="#" class="btn btn-success" style="width: 100%;">Pilih Mobil</a>
        </div>
      </div>
    </div>
    `;
  }
}

class Car extends Component {} 
